function cacheFunction(cb)
{
    let cache = {};
    function invoke(...parameters)
    {
        if(cache.hasOwnProperty(parameters))
        {
            console.log("The parameters are already present.");
            console.log(Object.keys(cache));
        }
        else
        {
            cb();
            cache[parameters] = "null";
        }
    }
    return invoke;
}

module.exports = cacheFunction;