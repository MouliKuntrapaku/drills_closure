function counterFactory(value)
{
    function increment(increment_value)
    {
        let result = value + increment_value;
        return result;
    }
    function decrement(decrement_value)
    {
        let result = value - decrement_value;
        return result;
    }
    return {Increment:increment,Decrement:decrement}
}

module.exports = counterFactory;