function limitFunctionCallCount(cb, number)
{
    let actual_number = number;
    function invoke()
    {
        if(number > 0)
        {
            cb();
            number-=1;
        }
        else
        {
            console.log(`The callback function is already called ${actual_number} times.`);
        }
    }
    return invoke
}

module.exports = limitFunctionCallCount;