const cacheFunction = require("../cacheFunction");

let cb = () => console.log("The parameters were not used earlier");

let returnedFunction = cacheFunction(cb);

returnedFunction(10);
returnedFunction(20);
returnedFunction(10,20);
returnedFunction(20,10);
returnedFunction(10,20,30);
returnedFunction(10,20);