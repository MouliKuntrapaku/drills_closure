const limitFunctionCallCount = require("../limitFunctionCallCount");

let cb = () => console.log("The callback function is invoked.");

let invoking = limitFunctionCallCount(cb,5);

invoking();
invoking();
invoking();
invoking();
invoking();
invoking();
invoking();